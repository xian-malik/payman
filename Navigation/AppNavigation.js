import React from 'react';
import { Text, TouchableOpacity, Animated, Easing, Image } from 'react-native';
import { StackNavigator, TabNavigator, TabBarBottom , DrawerNavigator } from 'react-navigation';
import { Fonts } from "../assets/utils/fonts";
import { Icon } from 'react-native-elements';

import Home from '../Containers/Home';

import Dashboard from '../Containers/Dashboard';
import BalanceTransfer from "../Containers/BalanceTransfer";
import CheckBalance from "../Containers/CheckBalance";
import Payment from "../Containers/Payment";
import MobileRefill from "../Containers/MobileRefill";
import CashOut from "../Containers/CashOut";
import Faq from "../Containers/Faq";

import DrawerContainer from '../Containers/DrawerContainer';

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
});

const drawerButton = (navigation) =>
    <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
            if (navigation.state.index === 0) navigation.navigate('DrawerOpen')
            else navigation.navigate('DrawerClose')
        }}
        style={{ margin: 13 }}>
        <Icon name='menu' color={'#FFF'} />
    </TouchableOpacity>
;

const TabStack = TabNavigator({
    CheckBalance: { screen: CheckBalance },
    BalanceTransfer: { screen: BalanceTransfer },
    Payment: { screen: Payment },
    MobileRefill: { screen: MobileRefill },
    CashOut: { screen: CashOut },
}, {
    gesturesEnabled: false,
    tabBarOptions: {
        activeBackgroundColor: '#31d769',
        inactiveBackgroundColor: '#F9F9F9',
        showLabel: false,
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
});

const DashboardStack = StackNavigator({
    Dashboard: { screen: Dashboard },
    TabNav: { screen: TabStack },
    Faq: { screen: Faq },
}, {
    headerMode: 'screen',
    navigationOptions: ({ navigation }) => ({
        headerStyle: {
            backgroundColor: '#31d769',
        },
        title: 'PAYMAN',
        headerTitleStyle: {
            fontWeight: '100',
            width: '80%',
            textAlign: 'center',
        },
        headerTintColor: 'white',
        gesturesEnabled: false,
        headerLeft: drawerButton(navigation),
    })
});
const DrawerStack = DrawerNavigator({
    TabStack: { screen: DashboardStack },
}, {
    initialRouteName: 'TabStack',
    gesturesEnabled: false,
    contentComponent: DrawerContainer
});


const DrawerNavigation = StackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
    headerMode: 'none',
    navigationOptions: ({navigation}) => ({
        headerStyle: {
            backgroundColor: '#31d769',
        },
        title: 'PAYMAN',
        headerTitleStyle: {
            fontWeight: '100',
            width: '80%',
            textAlign: 'center',
        },
        headerTintColor: 'white',
        gesturesEnabled: false,
        headerLeft: drawerButton(navigation),
    })
});

// login stack
const LoginStack = StackNavigator({
  Home: { screen: Home },
}, { headerMode: 'none' });

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  loginStack: { screen: LoginStack },
  drawerStack: { screen: DrawerNavigation }
}, {
  // Default config for all screens
  headerMode: 'none',
  title: 'Main',
  initialRouteName: 'loginStack',
  transitionConfig: noTransitionConfig
});

export default PrimaryNav;

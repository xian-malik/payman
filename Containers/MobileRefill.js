import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableHighlight, Alert } from 'react-native';
import { Fonts } from '../assets/utils/fonts';
import { Icon, Button, FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import PhoneInput from 'react-native-phone-input';

export default class MobileRefill extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Mobile Refill',
        tabBarIcon:  ({ focused }) => (
            <Icon name='phone-android' color={ focused ? '#FFF':'#333'} />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {
            phoneNo: '',
            rechargeAmount: '',
            transMessage: ''
        };
        this.confirmTransfer = this.confirmTransfer.bind(this);
    }
    confirmTransfer = () => {
        if ( this.state.rAccNo != '' && this.state.rAmount != '' ) {
            Alert.alert(
                'Confirm Transection To:',
                'Phone No: 254-505-855-841 \nName: PayMan \nAmount: 200',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => this.setState({transMessage: 'Recharge Complete'})},
                ],
                { cancelable: false }
            )
        } else {
            Alert.alert(
                'Invalid Credentials',
                'The information inserted is invalid',
                [
                    {text: 'OK'},
                    {text: ''},
                ],
                { cancelable: false }
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={globals.section}>
                    <Text style={styles.h2}>Input vendor information</Text>
                    <FormLabel>Phone Number</FormLabel>
                    <PhoneInput
                        initialCountry={'bd'}
                        style={{paddingVertical: 10, paddingHorizontal: 20}}
                        ref={(ref) => { this.phone = ref; }}
                        onChangeText={ (phoneNo) => this.setState({phoneNo}) }
                        value={this.state.phoneNo}/>
                    <FormLabel>Amount</FormLabel>
                    <FormInput
                        value={this.state.rAmount}
                        onChangeText={ (rechargeAmount) => this.setState({rechargeAmount}) }
                        keyboardType = 'numeric'
                        placeholder={'Enter Recharge Amount'}
                    />
                    <Button
                        raised small
                        onPress={this.confirmTransfer}
                        backgroundColor={'#31d769'}
                        rightIcon={{name: 'phone-android', size: 14 }}
                        fontSize={14}
                        title='Recharge Phone' />
                    <Text style={{marginTop: 20, textAlign: 'center'}}>{this.state.transMessage}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    balance: {
        fontSize: 64,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        color: "#31d769",
    },
    h2: {
        fontSize: 24,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        marginBottom: 20,
    },
    logo: {
        height: 50,
        width: "60%",
        marginBottom: 90
    },
});

const globals = StyleSheet.create({
    section: {
        width: '100%',
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#f9f9f9',
    },
});

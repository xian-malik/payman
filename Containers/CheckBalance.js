import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, TextInput, Image  } from 'react-native';
import { Fonts } from '../assets/utils/fonts';
import { Icon } from 'react-native-elements';

export default class CheckBalance extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Check Balance',
        tabBarIcon:  ({ focused }) => (
            <Icon name='attach-money' color={ focused ? '#FFF':'#333'} />
        ),
    };
    constructor(props) {
        super(props);
        this.state = { currentBalance: '30' };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={globals.section}>
                    <Text style={styles.bigP}>Your current balance is</Text>
                    <Text style={styles.balance}>$ {this.state.currentBalance}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    balance: {
        fontSize: 64,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        color: "#31d769",
    },
    bigP: {
        fontSize: 14,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
    },
    logo: {
        height: 50,
        width: "60%",
        marginBottom: 90
    },
});

const globals = StyleSheet.create({
    section: {
        width: '100%',
        padding: 20,
        backgroundColor: '#f9f9f9',
        borderBottomWidth: 1,
        borderBottomColor: '#f9f9f9',
    },
});

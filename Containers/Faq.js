import React from 'react';
import { StyleSheet, ScrollView, Text, View, Image, TextInput, TouchableHighlight, Alert } from 'react-native';
import { Fonts } from '../assets/utils/fonts';
import { Card, Icon, Button, FormLabel, FormInput, FormValidationMessage} from 'react-native-elements';

export default class Faq extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'FAQ',
        tabBarIcon:  ({ focused }) => (
            <Icon name='question-answer' color={ focused ? '#FFF':'#333'} />
        ),
    };

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={globals.section}>
                    <View style={{padding: 20}}>
                        <Text style={styles.h2}>FAQs</Text>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                        <Card
                            style={{textAlign: 'left'}}
                            title='Faq Question #1'>
                            <Text>
                            The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    h2: {
        fontSize: 24,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        marginBottom: 20,
    },
});

const globals = StyleSheet.create({
    section: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#f9f9f9',
    },
});

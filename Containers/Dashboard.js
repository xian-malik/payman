import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import { Fonts } from "../assets/utils/fonts";
import { Icon } from 'react-native-elements';

export default class Dashboard extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                        <View style={styles.dashboardContent}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('CheckBalance')} >
                                <Icon name='attach-money' color={'#31d769'} size={36} />
                                <Text style={styles.items}>CHECK BALANCE</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('BalanceTransfer')} >
                                <Icon name='send' color={'#31d769'} size={36} />
                                <Text style={styles.items}>BALANCE TRANSFER</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('Payment')} >
                                <Icon name='store' color={'#31d769'} size={36} />
                                <Text style={styles.items}>PAYMENT</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('MobileRefill')} >
                                <Icon name='phone-android' color={'#31d769'} size={36} />
                                <Text style={styles.items}>MOBILE REFILL</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('CashOut')} >
                                <Icon name='call-missed-outgoing' color={'#31d769'} size={36} />
                                <Text style={styles.items}>CASH OUT</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('Faq')} >
                                <Icon name='question-answer' color={'#31d769'} size={36} />
                                <Text style={styles.items}>FAQ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        height: '100%',
        justifyContent: 'center',
    },
    dashboardHeader: {
        backgroundColor: '#FAFAFA',
        height: 150,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dashboardContent: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'flex-start',
        padding: 30,
        backgroundColor: '#FFFFFF',
    },
    button: {
        margin: '2.5%',
        padding: 15,
        width: '45%',
        aspectRatio: 1,
        backgroundColor: '#fafafb',
        borderRadius: 10,
        shadowOffset: { width: 2, height: 5 },
        shadowColor: '#333',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    items: {
        textAlign: 'center',
        color: '#707070',
        marginTop: 15,
    },
});

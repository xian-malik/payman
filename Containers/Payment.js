import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableHighlight, Alert } from 'react-native';
import { Fonts } from '../assets/utils/fonts';
import { Icon, Button, FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'

export default class Payment extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Payment',
        tabBarIcon:  ({ focused }) => (
            <Icon name='store' color={ focused ? '#FFF':'#333'} />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {
            rAccNo: '',
            rAmount: '',
            transMessage: ''
        };
        this.confirmTransfer = this.confirmTransfer.bind(this);
    }
    confirmTransfer = () => {
        if ( this.state.rAccNo != '' && this.state.rAmount != '' ) {
            Alert.alert(
                'Confirm Transection To:',
                'Account No: 254-505-855-841 \nName: PayMan \nAmount: 2000',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => this.setState({transMessage: 'Transection Complete'})},
                ],
                { cancelable: false }
            )
        } else {
            Alert.alert(
                'Invalid Credentials',
                'The information inserted is invalid',
                [
                    {text: 'OK'},
                    {text: ''},
                ],
                { cancelable: false }
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={globals.section}>
                    <Text style={styles.h2}>Input vendor information</Text>
                    <FormLabel>Name</FormLabel>
                    <FormInput
                        value={this.state.rAccNo}
                        onChangeText={ (rAccNo) => this.setState({rAccNo}) }
                        placeholder={'Enter Account No'}
                    />
                    <FormLabel>Amount</FormLabel>
                    <FormInput
                        value={this.state.rAmount}
                        onChangeText={ (rAmount) => this.setState({rAmount}) }
                        keyboardType = 'numeric'
                        placeholder={'Enter Account No'}
                    />
                    <Button
                        raised small
                        onPress={this.confirmTransfer}
                        backgroundColor={'#31d769'}
                        rightIcon={{name: 'send', size: 14 }}
                        fontSize={14}
                        title='Complete Payment' />
                    <Text style={{marginTop: 20, textAlign: 'center'}}>{this.state.transMessage}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    balance: {
        fontSize: 64,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        color: "#31d769",
    },
    h2: {
        fontSize: 24,
        fontFamily: Fonts.RobotoLight,
        textAlign: 'center',
        marginBottom: 20,
    },
    logo: {
        height: 50,
        width: "60%",
        marginBottom: 90
    },
});

const globals = StyleSheet.create({
    section: {
        width: '100%',
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#f9f9f9',
    },
});

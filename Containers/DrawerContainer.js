import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Image, ScrollView, View } from 'react-native';
import { DrawerItems, SafeAreaView, NavigationActions } from 'react-navigation';
import { Icon, Avatar } from 'react-native-elements';
import { Fonts } from "../assets/utils/fonts";

export default class DrawerContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'Zubayer',
            accountno: 'X123-124-156-642'
        }
    }
    logout = () => {
        const actionToDispatch = NavigationActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: 'loginStack' })]
        });
        this.props.navigation.dispatch(actionToDispatch);
    }

  render() {
    const { navigation } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.drawerHeadContainer}>
            <View style={styles.drawerHeadFlex}>
                <Avatar
                    large
                    rounded
                    source={require('./images/blank-avatar.png')}
                    activeOpacity={1}
                />
                <View style={styles.drawerDesc}>
                    <Text style={styles.drawerHead}>
                        {this.state.username.toUpperCase()}
                    </Text>
                    <Text style={styles.drawerMeta}>
                        {this.state.accountno}
                    </Text>
                </View>
            </View>
        </View>
        <View style={styles.drawerItemsContainer}>
            <TouchableOpacity
              style={styles.drawerItems}
              onPress={() => navigation.navigate('Dashboard')}>
              <Icon
                  name='dashboard'
                  color='#31d769' />
              <Text style={styles.drawerItem}>Dashbaord</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.drawerItems}
              onPress={() => navigation.navigate('Dashboard')}>
              <Icon
                  name='settings'
                  color='#31d769' />
                <Text style={styles.drawerItem}>Settings</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.drawerItems}
              onPress={() => navigation.navigate('Faq')}>
              <Icon
                  name='question-answer'
                  color='#31d769' />
                <Text style={styles.drawerItem}>FAQ</Text>
            </TouchableOpacity>
            <View style={{ borderBottomColor: '#f2f2f2', borderBottomWidth: 1, marginVertical: 10 }} />
            <TouchableOpacity
              style={styles.drawerItems}
              onPress={this.logout}>
              <Icon
                  name='exit-to-app'
                  color='#31d769' />
                <Text style={styles.drawerItem}>Log Out</Text>
            </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    drawerHeadContainer: {
        backgroundColor: "#31d769",
        height: 200,
        padding: 15,
        justifyContent: 'flex-end',
    },
    drawerHeadFlex: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    drawerDesc: {
        flex: 1,
        paddingHorizontal: 15,
    },
    drawerHead: {
        fontSize: 16,
        color: '#FFF',
        letterSpacing: 1,
        fontFamily: Fonts.RobotoMedium,
    },
    drawerMeta: {
        fontSize: 12,
        color: '#F9F9F9'
    },
    drawerItemsContainer: {
        paddingTop: 10,
    },
    drawerItem: {
        fontSize: 14,
        marginLeft: 20,
    },
    drawerItems: {
        flex: 1,
        paddingHorizontal: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 15,
        paddingBottom: 15,
    }
})

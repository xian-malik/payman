import React from 'react';
import {StyleSheet, Text, View, TouchableHighlight, TextInput, Image, ActivityIndicator , FlatList } from 'react-native';
import { Fonts } from '../assets/utils/fonts';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            error_msg: '',
            isLoading: true,
            dataSource: [],
        };
    }
    componentDidMount(){
        return fetch('http://192.168.0.106:80/api/user/read.php?id={$username}')
        .then((responseJson) => {
            if (responseJson.ok) {
                this.setState({
                    isLoading: false,
                    dataSource: JSON.parse(responseJson._bodyInit),
                });
            } else {
            }
        })
        .catch((error) =>{
            console.log(error);
        });
    }

    validateLogin = () => {
        if ( (this.state.username === this.state.dataSource.user_id && this.state.password === this.state.dataSource.password) ) {
            this.props.navigation.navigate('drawerStack');
            this.setState({username: '', password: ''});
        } else {
            this.setState({error_msg: "Authentication Failed"});
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator />
                </View>
            );
        } else {
            let users = this.state.dataSource.map( (val, key) => {
                return <Text key={key}> { val.user_id } </Text>
            });
            return (
                <View style={styles.container}>
                    <Text style={styles.textLogo}>PAYMAN</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Username"
                        value={this.state.username}
                        onChangeText={ (username) => this.setState({username}) }
                        autoCapitalize='none'
                        />
                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        value={this.state.password}
                        onChangeText={ (password) => this.setState({password}) }
                        autoCapitalize='none'
                        secureTextEntry
                        />
                    <TouchableHighlight
                        onPress={this.validateLogin} >
                        <Text
                            style={styles.button} >
                            LOGIN
                        </Text>
                    </TouchableHighlight>
                    <Text style={styles.errorMsg}>{ this.state.error_msg }</Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
      fontSize: 28,
      fontFamily: Fonts.RobotoBold,
    },
    errorMsg: {
        fontSize: 12,
        fontFamily: Fonts.RobotoLight,
        letterSpacing: 1,
        color: "#ed143d",
        marginTop: 30
    },
    textLogo: {
        fontSize: 48,
        fontFamily: Fonts.RobotoBlack,
        letterSpacing: 7,
        color: "#31d769",
        marginBottom: 90
    },
    logo: {
        height: 50,
        width: "60%",
        marginBottom: 90
    },
    input: {
        fontFamily: Fonts.RobotoLight,
        backgroundColor: "#f9f9f9",
        color: "#333",
        textAlign: 'center',
        letterSpacing: 1,
        height: 40,
        width: 300,
        padding: 5,
        marginBottom: 10,
        textDecorationStyle: 'solid'
    },
    button: {
        fontFamily: Fonts.RobotoLight,
        backgroundColor: "#31d769",
        color: "#FFF",
        textAlign: 'center',
        textAlignVertical: "center",
        letterSpacing: 1,
        height: 40,
        width: 300,
        padding: 5
    },
});

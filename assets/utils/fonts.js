export const Fonts = {
  RobotoBold: "Roboto-Bold",
  RobotoBlack: "Roboto-Black",
  RobotoMedium: "Roboto-Medium",
  RobotoLight: "Roboto-Light",
  RobotoThin: "Roboto-Thin"
}

import React, {Component} from 'react'
import { View, StatusBar, Alert } from 'react-native';
import { Provider } from 'react-redux';
import createStore from './Redux';

import ReduxNavigation from './Navigation/ReduxNavigation';

const store = createStore();

export default class App extends Component {
    render() {
        return (
          <Provider store={store}>
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
              <StatusBar barStyle='light-content' backgroundColor={'#00a43b'} />
              <ReduxNavigation />
            </View>
          </Provider>
        );
    }
}
